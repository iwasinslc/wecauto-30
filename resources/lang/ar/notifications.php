<?php
return [
    'order_created'       => 'تم إنشاء الطلب رقم :id :amount currency: بنجاح',
    'order_closed'        => 'تم إغلاق الطلب # :amount :currency id: بسبب نقص الأموال في الميزانية العمومية',
    'sale'                => 'تم بيع :currency amount: بنجاح',
    'purchase'            => 'شراء تم بيع :currency amount: بنجاح',
    'partner_accrue'      => 'لقد تلقيت للتو عمولة تابعة :amount من المستخدم :login في المستوى :level',
    'wallet_refiled'      => ':currency :amount تمت إضافة محفظتك إلى',
    'rejected_withdrawal' => ':currency :amount تم إلغاء استنتاجك بمبلغ',
    'approved_withdrawal' => ':currency :amount تم تأكيد استنتاجك بمبلغ',
    'new_partner'         => 'لديك شريك جديد :login على المستوى :level',
    'parking_bonus'       => ':currency :amount مكافأة وقوف السيارات',
    'licence_cash_back'   => ':currency :amount استرداد نقدي لشراء ترخيص'
];