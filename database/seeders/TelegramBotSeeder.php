<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

/**
 * Class TelegramBotSeeder
 */
class TelegramBotSeeder extends Seeder
{
    /**
     * @throws \Exception
     */
    public function run()
    {
        $data = [
            /**
             * Admin bot
             */
            [
                'bot_keyword'    => 'admin_bot',
                'command'        => '/start',
                'description'    => 'command',
                'method_address' => 'admin_bot\AdminBotBotStartController',
                'created_at'     => now(),
            ],

            /**
             * Notification bot
             */
            [
                'bot_keyword'    => 'notification_bot',
                'command'        => '/start',
                'description'    => 'command',
                'method_address' => 'notification_bot\NotificationBotStartController',
                'created_at'     => now(),
            ],
            [
                'bot_keyword'    => 'notification_bot',
                'command'        => '/auth',
                'description'    => 'command',
                'method_address' => 'notification_bot\NotificationBotAuthController',
                'created_at'     => now(),
            ],
            [
                'bot_keyword'    => 'notification_bot',
                'command'        => '/enter_password',
                'description'    => 'command',
                'method_address' => 'notification_bot\NotificationBotEnterPasswordController',
                'created_at'     => now(),
            ],

            /**
             * Account bot
             */

            /**
             * Russian
             */

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '/auth',
                'description'    => 'command',
                'method_address' => 'account_bot\Auth\IndexController',
                'created_at'     => now(),
            ],
            [
                'bot_keyword'    => 'account_bot',
                'command'        => '/enter_login',
                'description'    => 'command',
                'method_address' => 'account_bot\Auth\AuthController',
                'created_at'     => now(),
            ],
            [
                'bot_keyword'    => 'account_bot',
                'command'        => '/enter_password',
                'description'    => 'command',
                'method_address' => 'account_bot\Auth\EnterPasswordController',
                'created_at'     => now(),
            ],
            [
                'bot_keyword'    => 'account_bot',
                'command'        => '/start',
                'description'    => 'command',
                'method_address' => 'account_bot\StartController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '🏠 На главную',
                'description'    => 'command',
                'method_address' => 'account_bot\StartController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => 'set_lang',
                'description'    => 'command',
                'method_address' => 'account_bot\LangController',
                'created_at'     => now(),
            ],
            [
                'bot_keyword'    => 'account_bot',
                'command'        => '🇬🇧 Язык',
                'description'    => 'command',
                'method_address' => 'account_bot\LangController',
                'created_at'     => now(),
            ],
            [
                'bot_keyword'    => 'account_bot',
                'command'        => 'login',
                'description'    => 'command',
                'method_address' => 'account_bot\Settings\PersonalData\LoginController',
                'created_at'     => now(),
            ],
            [
                'bot_keyword'    => 'account_bot',
                'command'        => '🎤 Логин',
                'description'    => 'command',
                'method_address' => 'account_bot\Settings\PersonalData\LoginController',
                'created_at'     => now(),
            ],
            [

                'bot_keyword'    => 'account_bot',
                'command'        => 'enter_new_email',
                'description'    => 'command',
                'method_address' => 'account_bot\Settings\PersonalData\SetupNewEmail\EnterNewEmailController',
                'created_at'     => now(),
            ],
            [

                'bot_keyword'    => 'account_bot',
                'command'        => '📧 E-mail',
                'description'    => 'command',
                'method_address' => 'account_bot\Settings\PersonalData\EmailController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => 'send_new_email_verification_code',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Settings\PersonalData\ConfirmEmail\SendNewCodeController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '📣 Имя',
                'description'    => 'command',
                'method_address' => 'account_bot\Settings\PersonalData\NameController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '🈁 Пароль',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Settings\PersonalData\SetupPasswordController',
                'created_at'     => now(),
            ],
            [
                'bot_keyword'    => 'account_bot',
                'command'        => '💰 Кошельки',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Settings\PersonalData\Wallets\IndexController',
                'created_at'     => now(),
            ],
            [
                'bot_keyword'    => 'account_bot',
                'command'        => 'settings_wallet',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Settings\PersonalData\Wallets\WalletController',
                'created_at'     => now(),
            ],


            [
                'bot_keyword'    => 'account_bot',
                'command'        => '📥 Вложить',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Topup\IndexController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => 'topup_currency',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Topup\CurrencyController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => 'topup_amount',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Topup\AmountController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => 'topup_rate',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Topup\RateController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '📤 Вывести',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Withdraw\IndexController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '💼 Личный кабинет',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\IndexController',
                'created_at'     => now(),
            ],


            [
                'bot_keyword'    => 'account_bot',
                'command'        => '💳 Мои вклады',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Deposits\IndexController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '✅ Активные депозиты',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Deposits\ActiveController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '❌ Закрытые депозиты',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Deposits\ClosedController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '🔧 Настройки',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Settings\IndexController',
                'created_at'     => now(),
            ],



            [
                'bot_keyword'    => 'account_bot',
                'command'        => '🕐 Статистика',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Statistic\IndexController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '💪 Партнеры',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Partners\IndexController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '👤 Партнер',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Partners\PartnerController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '👥 Рефералы',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Partners\ReferralsController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '💬 Промоматериалы',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Promo\IndexController',
                'created_at'     => now(),
            ],


            [
                'bot_keyword'    => 'account_bot',
                'command'        => '💰 Инвестиционные планы',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Rates\IndexController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '💬 О нас',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\AboutUsController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '☎ Контакты',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\ContactsController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => 'create_deposit',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Rates\AddController',
                'created_at'     => now(),
            ],




            [
                'bot_keyword'    => 'account_bot',
                'command'        => 'create_depo_wallet',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Rates\SumController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => 'finish_deposit',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Rates\BuyController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => 'withdraw_wallet',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Withdraw\SumController',
                'created_at'     => now(),
            ],


            /**
             * English
             */

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '🈁 Password',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Settings\PersonalData\SetupPasswordController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '🏠 Home',
                'description'    => 'command',
                'method_address' => 'account_bot\StartController',
                'created_at'     => now(),
            ],
            [
                'bot_keyword'    => 'account_bot',
                'command'        => '🇬🇧 Language',
                'description'    => 'command',
                'method_address' => 'account_bot\LangController',
                'created_at'     => now(),
            ],
            [
                'bot_keyword'    => 'account_bot',
                'command'        => '🎤 Login',
                'description'    => 'command',
                'method_address' => 'account_bot\Settings\PersonalData\LoginController',
                'created_at'     => now(),
            ],
            [

                'bot_keyword'    => 'account_bot',
                'command'        => '📧 E-mail',
                'description'    => 'command',
                'method_address' => 'account_bot\Settings\PersonalData\EmailController',
                'created_at'     => now(),
            ],
            [
                'bot_keyword'    => 'account_bot',
                'command'        => '📣 Name',
                'description'    => 'command',
                'method_address' => 'account_bot\Settings\PersonalData\NameController',
                'created_at'     => now(),
            ],
            [
                'bot_keyword'    => 'account_bot',
                'command'        => '💰 Wallets',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Settings\PersonalData\Wallets\IndexController',
                'created_at'     => now(),
            ],
            [
                'bot_keyword'    => 'account_bot',
                'command'        => '📥 Invest',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Topup\IndexController',
                'created_at'     => now(),
            ],
            [
                'bot_keyword'    => 'account_bot',
                'command'        => '📤 Withdraw',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Withdraw\IndexController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '💼 User area',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\IndexController',
                'created_at'     => now(),
            ],


            [
                'bot_keyword'    => 'account_bot',
                'command'        => '💳 Investments',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Deposits\IndexController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '✅ Active deposits',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Deposits\ActiveController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '❌ Closed deposits',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Deposits\ClosedController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '🔧 Settings',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Settings\IndexController',
                'created_at'     => now(),
            ],



            [
                'bot_keyword'    => 'account_bot',
                'command'        => '🕐 Stats',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Statistic\IndexController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '💪 Partners',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Partners\IndexController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '👤 Referral',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Partners\PartnerController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '👥 Referrals',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Partners\ReferralsController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '💬 Promo',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Promo\IndexController',
                'created_at'     => now(),
            ],


            [
                'bot_keyword'    => 'account_bot',
                'command'        => '💰 Pay plans',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Rates\IndexController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '💬 About us',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\AboutUsController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '☎ Contacts',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\ContactsController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '📤 Withdraw',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Withdraw\IndexController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '💼 User area',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\IndexController',
                'created_at'     => now(),
            ],


            [
                'bot_keyword'    => 'account_bot',
                'command'        => '💳 Investments',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Deposits\IndexController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '✅ Active deposits',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Deposits\ActiveController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '❌ Closed deposits',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Deposits\ClosedController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '🔧 Settings',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Settings\IndexController',
                'created_at'     => now(),
            ],



            [
                'bot_keyword'    => 'account_bot',
                'command'        => '🕐 Stats',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Statistic\IndexController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '💪 Partners',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Partners\IndexController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '👤 Referral',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Partners\PartnerController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '👥 Referrals',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Partners\ReferralsController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '💬 Promo',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Cabinet\Promo\IndexController',
                'created_at'     => now(),
            ],


            [
                'bot_keyword'    => 'account_bot',
                'command'        => '💰 Pay plans',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Rates\IndexController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '💬 About us',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\AboutUsController',
                'created_at'     => now(),
            ],

            [
                'bot_keyword'    => 'account_bot',
                'command'        => '☎ Contacts',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\ContactsController',
                'created_at'     => now(),
            ],


            [
                'bot_keyword'    => 'account_bot',
                'command'        => 'select_type',
                'description'    => 'keyboard button',
                'method_address' => 'account_bot\Rates\TypeController',
                'created_at'     => now(),
            ],
        ];

        foreach ($data as $row) {
            $checkExists = \App\Models\Telegram\TelegramBotScopes::where('bot_keyword', $row['bot_keyword'])
                ->where('command', $row['command'])
                ->where('method_address', $row['method_address'])
                ->count();

            if ($checkExists > 0) {
                echo "Telegram command '".$row['command']."' already registered.\n";
                continue;
            }

            \App\Models\Telegram\TelegramBotScopes::create($row);
            echo "Telegram command '".$row['command']."' registered.\n";
        }
    }
}
