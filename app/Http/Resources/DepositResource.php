<?php

namespace App\Http\Resources;

use App\Models\Deposit;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     title="DepositResource",
 *     description="Deposit resource",
 *     type="object",
 *     @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Deposit"))
 * )
 */
class DepositResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var $this Deposit */
        return [
            'id' => $this->id,
            'value' => $this->invested,
            'currency' => $this->currency->code,
            'duration' => $this->duration,
            'status' => $this->active ? 'active' : 'not active',
            'fst' => [ // Fst information
                'days' => $this->fast_days,
                'value' => $this->fst_in_usd,
                'currency' => 'USD'
            ],
            'closing_at' => $this->datetime_closing->format('Y-m-d H:i:s'),
            'created_at' => $this->created_at->format('Y-m-d H:i:s')
        ];
    }
}
