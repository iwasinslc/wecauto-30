<?php
namespace App\Http\Requests;

use App\Rules\RuleEnoughBalance;
use App\Rules\RuleHasLicence;
use App\Rules\RuleTransferEnoughBalance;
use App\Rules\RuleUUIDEqual;
use App\Rules\RuleWalletExist;
use App\Rules\RuleWalletWithExternalAddress;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RequestWithdraw
 * @package App\Http\Requests
 *
 * @property string wallet_id
 * @property float amount
 * @property string captcha
 */
class RequestTransfer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'email'      => 'required|email',
            'login'      => 'required|string',
            'wallet_id' => ['required', new RuleUUIDEqual],
            'amount'    => ['numeric', new RuleHasLicence, new RuleTransferEnoughBalance, 'min:0.0001', 'max:1000000'],
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => __('Email is required'),
            'email.email' => __('Wrong email format.'),
            'login.required' => __('Login is required'),
            'wallet_id.required' => __('Wallet is required'),
            'amount.numeric'     => __('Amount have to be numeric'),

        ];
    }
}
