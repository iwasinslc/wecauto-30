<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RequestCreateProject
 * @package App\Http\Requests
 *
 * @property string name
 * @property string curator_contacts
 * @property string url
 * @property string video_description
 * @property string description
 * @property string currency
 */
class RequestCreateProject extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required',
            'curator_contacts'      => 'required',
            'url'                   => '',
            'logotype'              => !\Route::is('admin.projects.*') ? 'required|dimensions:min_width=160,min_height=120,max:500' : '',
            'video_description'     => '',
            'description'           => 'required|min:10',
            'marketing_description' => 'required|min:10',
            'currency'              => '',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'                  => 'Название проекта обязательно к заполнению',
            'curator_contacts.required'      => 'Контакты куратора обязательны к заполнению',
            'description.required'           => 'Описание проекта обязательно к заполнению',
            'marketing_description.required' => 'Описание маркетинга проекта обязательно к заполнению',
            'currency.required'              => 'Выберите платежную систему для оплаты',
            'logotype.required'              => 'Выберите файл логотипа',
        ];
    }
}
