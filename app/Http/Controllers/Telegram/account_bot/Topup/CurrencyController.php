<?php
namespace App\Http\Controllers\Telegram\account_bot\Topup;

use App\Helpers\Constants;
use App\Http\Controllers\Controller;
use App\Models\PaymentSystem;
use App\Models\Telegram\TelegramBotEvents;
use App\Models\Telegram\TelegramBots;
use App\Models\Telegram\TelegramBotScopes;
use App\Models\Telegram\TelegramUsers;
use App\Models\Telegram\TelegramWebhooks;
use App\Modules\Messangers\TelegramModule;

class CurrencyController extends Controller
{
    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function index(TelegramWebhooks $webhook,
                          TelegramBots $bot,
                          TelegramBotScopes $scope,
                          TelegramUsers $telegramUser,
                          TelegramBotEvents $event)
    {
        TelegramModule::setLanguageLocale($telegramUser->language);

        preg_match('/topup\_currency '.Constants::UUID_REGEX.'/', $event->text, $data);

        if (isset($data[1]))
        {
            $payment_system_id = $data[1];
        }
        else {
            return response('ok');
        }

        $user = $telegramUser->user;

        cache()->put('topup_data'.$user->id, [
            'payment_system_id'=>$payment_system_id
        ] , 30 );

        $paymentSystem = PaymentSystem::find($payment_system_id);

        if (null === $paymentSystem) {
            throw new \Exception('PS can not be found.');
        }



        $message = view('telegram.account_bot.topup.currency', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
            'event'        => $event,
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }

        $keyboard = [];

        foreach ($paymentSystem->currencies as $currency) {
                $keyboard[] = [
                    ['text' => $currency->code, 'callback_data' => 'topup_rate '.$currency->id],
                ];

        }

        \Log::info(print_r($keyboard,true));

        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage($event->chat_id,
                $message,
                'HTML',
                true,
                false,
                null,
                [
                    'inline_keyboard' => $keyboard,
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true,
                ],
                $scope,
                'inline_keyboard');
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }

        return response('ok');
    }
}