<?php
namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Models\Deposit;
use App\Models\Licences;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $licences = Licences::with(['currency'])->orderBy('price')->get();
        $user = user();
        $deposits = cache()->tags('userDeposits.' . $user->id)->remember('c.' . $user->id . '.userDeposits.active-' . 1, getCacheCLifetime('userDeposits'), function () use ($user) {
            $deposits = Deposit::where('user_id', $user->id)->where('active', 1);

            return $deposits->get();
        });

        $partners = cache()->tags('userLastPartners.' . user()->id)->remember('c.' . user()->id . '.userLastPartners',now()->addDay(),  function () {
            return user()->referrals()->withPivot(['line'])->orderBy('created_at')->limit(7)->get();
        });

        return view('profile.profile', [
            'licences'=>$licences,
            'deposits' => $deposits,
            'partners'=>$partners
        ]);
    }




    public function delete_notification($id)
    {
        $notification = user()->notifications()->findOrFail($id);
        $notification->delete();
        return response()->json(['response'=>'ok']);
    }


}
