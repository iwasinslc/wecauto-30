<?php
namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestBuyWEC;
use App\Http\Requests\RequestCharity;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\TransactionType;

class BuyController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {


        if (!\user()->hasRole(['root']))
        {
            return response('ok');
        }

        return view('profile.buy');
    }

//
//    public function store(RequestBuyWEC $request)
//    {
//
//        $data = cache()->pull('protect-exchange-'.getUserId());
//
//        if ($data!==null)
//        {
//
//            return back()->with('error', __('Error'));
//        }
//
//        cache()->put('protect-exchange-'.getUserId(), '1', 0.1);
//
//
//        $rate = Setting::getValue('wec_price');
//
//
//
//        $limit = 10000;
//
//        $sum = user()->transactions()
//            ->where('type_id', TransactionType::getByName('buy_wec')->id)
//            ->where('created_at', '>=', now()->startOfDay()->toDateTimeString())
//            ->where('created_at', '<=', now()->toDateTimeString())
//            ->sum('amount');
//
//        if ($limit-$sum<$request->amount)
//        {
//            return back()->with('error', __('Your current purchase limit').' - '.(($limit-$sum)<=0 ? 0 : ($limit-$sum)).' WEC');
//        }
//
//        $wallet = user()->wallets()->find($request->wallet_id);
//        $wec_wallet= user()->getUserWallet('WEC');
//
//
//
//        if ($wallet->balance < $request->amount*$rate) {
//            return back()->with('error', __('Requested amount exceeds the wallet balance'));
//        }
//
//
//        try {
//            Transaction::buy_wec($wec_wallet,$wallet, $request->amount, $rate);
//        } catch(\Exception $e) {
//            return back()->with('error', $e->getMessage());
//        }
//
//        return back()->with('success', __('Exchange successful'));
//    }
}
