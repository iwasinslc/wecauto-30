<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Deposit;
use App\Models\DepositQueue;
use App\Models\Rate;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;

/**
 * Class DepositController
 * @package App\Http\Controllers\Admin
 */
class DepositController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.deposits.index');
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function dataTable()
    {

    }

}
