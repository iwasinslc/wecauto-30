<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class PostLimitException extends Exception
{
    /**
     * Report the exception.
     *
     * @return void
     */
    public function report() // Not report this error
    {
        //
    }

    /**
     * Render exception
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function render($request)
    {
        if ($request->ajax()) {
            return response()->json([
                'message' => __('main.post_limit.reached')
            ], Response::HTTP_TOO_MANY_REQUESTS);
        } else {
            return back()->with('error', __('Слишком много запросов'));
        }
    }
}
