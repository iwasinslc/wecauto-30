<?php

namespace App\Console\Commands\Manual;

use App\Models\Currency;
use App\Models\Deposit;
use App\Models\Licences;
use App\Models\PaymentSystem;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Console\Command;

class UpdateDepositsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:deposits';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create test user from test-users.csv file';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Deposit::query()->where('created_at', '<', '2021-04-15 15:00:00')->update([
            'min_days' => 90,
            'start_percent'=>30,
            'max_fst'=>20
        ]);

        Deposit::query()->where('created_at', '>=', '2021-04-15 15:00:00')->update([
            'min_days' => 120,
            'start_percent'=>35,
            'max_fst'=>15
        ]);
    }
}
