<?php
namespace App\Models;

use App\Traits\ModelTrait;
use App\Traits\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Project
 * @package App\Models
 *
 * @property string id
 * @property string logotype_url
 * @property string name
 * @property string url
 * @property string description
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class Project extends Model
{
    use Uuids;
    use ModelTrait;

    /** @var bool $incrementing */
    public $incrementing = false;

    /** @var array $fillable */
    protected $fillable = [
        'logotype_url',
        'name',
        'url',
        'description',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class,'user_id');
    }


    public function addImg($file) {
        $folder          = 'projects';
        $destinationPath = public_path() . '/'.$folder.'/';

//        if (!empty($this->img)) {
//            @unlink($destinationPath . $this->img);
//        }

        $ext      = $file->getClientOriginalExtension() ?: 'png';
        $filename = str_random(20) . '.' . $ext;



        $path = $file->storeAs(
            'news', $filename, ['disk'=>'s3', 'visibility'=>'public']
        );

        $this->logotype_url = $path;
        $this->save();
    }

}
